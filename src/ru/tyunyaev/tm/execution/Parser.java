package ru.tyunyaev.tm.execution;

import ru.tyunyaev.tm.interfaces.ITerminalConst;

public class Parser implements ITerminalConst {
    private final String[] args;
    private String arg;

    public Parser(String[] args) {
        this.args = args;
    }

    public void startParseArgs() {
        System.out.println(WELCOME_TEXT);
        if (checkArgs()) { parseArgs(); }
        else { System.out.println("No arguments passed");}
    }

    private boolean checkArgs() {
        if (args == null || args.length == 0) {
            return false;
        } else {
            arg = args[0];
            return arg != null && !arg.isEmpty();
        }
    }

    private void parseArgs() {
        switch (arg) {
            case CMD_HELP:
                displayHelp();
                break;
            case CMD_VERSION:
                displayVersion();
                break;
            case CMD_ABOUT:
                displayAbout();
                break;
            default:
                System.out.println("Unknown argument");
                break;
        }
    }

    private void displayHelp(){
        System.out.println("[HELP]");
        System.out.println("version - " + VERSION_DESCRIPTION);
        System.out.println("about - " + ABOUT_DESCRIPTION);
        System.out.println("help - " + HELP_DESCRIPTION);
    }

    private void displayVersion() {
        System.out.println("[VERSION]");
        System.out.println("VERSION: " + VERSION);
    }

    private void displayAbout() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: " + FIRST_NAME + " " + LAST_NAME);
        System.out.println("E-MAIL: " + E_MAIL);
    }
}
