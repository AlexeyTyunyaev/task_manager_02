package ru.tyunyaev.tm;
import ru.tyunyaev.tm.execution.Parser;

public class Application {

    public static void main(String[] args) {
        Parser parser = new Parser(args);
        parser.startParseArgs();
    }
}
